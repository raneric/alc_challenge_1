package com.alc.alcchallenge1;

import android.net.http.SslError;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class AboutAlcActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_alc);
        Toolbar toolbar = (Toolbar) findViewById(R.id.about_alc_toolbar);
        setSupportActionBar(toolbar);

        WebView aboutAlc = (WebView) findViewById(R.id.about_alc_webview);
        WebSettings webSettings = aboutAlc.getSettings();

        webSettings.setJavaScriptEnabled(true);
        aboutAlc.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        });

        aboutAlc.loadUrl("https://andela.com/alc");

    }

}
